/*
 * This file is part of matrix-glib-sdk
 *
 * matrix-glib-sdk is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * matrix-glib-sdk is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with matrix-glib-sdk. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "matrix-message-text.h"
#include "matrix-enumtypes.h"
#include "matrix-types.h"

/**
 * SECTION:matrix-message-text
 * @short_description: message handler for plain text messages
 *
 * This is the default message handler for `m.text` messages.
 */
enum {
    PROP_0,
    PROP_FORMAT,
    PROP_FORMATTED_BODY,
    NUM_PROPERTIES
};

static GParamSpec *matrix_message_text_properties[NUM_PROPERTIES];

typedef struct {
    gchar *_format;
    gchar *_formatted_body;
} MatrixMessageTextPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(MatrixMessageText, matrix_message_text, MATRIX_MESSAGE_TYPE_BASE);

static void
matrix_message_text_real_from_json(MatrixMessageBase *matrix_message_base, JsonNode *json_data, GError **error)
{
    MatrixMessageTextPrivate *priv;
    JsonObject *root;
    JsonNode *node;
    GError *inner_error = NULL;

    g_return_if_fail(json_data != NULL);

    priv = matrix_message_text_get_instance_private(MATRIX_MESSAGE_TEXT(matrix_message_base));

    root = json_node_get_object(json_data);

    if ((node = json_object_get_member(root, "format")) != NULL) {
        g_free(priv->_format);
        priv->_format = g_strdup(json_node_get_string(node));
    }

    if ((node = json_object_get_member(root, "formatted_body")) != NULL) {
        g_free(priv->_formatted_body);
        priv->_formatted_body = g_strdup(json_node_get_string(node));
    }

    MATRIX_MESSAGE_BASE_CLASS(matrix_message_text_parent_class)->from_json(matrix_message_base, json_data, &inner_error);

    if (inner_error != NULL) {
        g_propagate_error(error, inner_error);
    }
}

static void
matrix_message_text_real_to_json(MatrixMessageBase *matrix_message_base, JsonNode *json_data, GError **error)
{
    MatrixMessageTextPrivate *priv;
    JsonObject *root;
    GError *inner_error = NULL;

    g_return_if_fail(json_data != NULL);

    priv = matrix_message_text_get_instance_private(MATRIX_MESSAGE_TEXT(matrix_message_base));

    if (priv->_format == NULL && priv->_formatted_body != NULL) {
        g_set_error(error, MATRIX_ERROR, MATRIX_ERROR_INCOMPLETE,
                "Won't generate a m.text message JSON that contains formatted_body, but not format");
        return;
    }

    root = json_node_get_object(json_data);

    if (priv->_format != NULL)
        json_object_set_string_member(root, "format", priv->_format);
    if (priv->_formatted_body != NULL)
        json_object_set_string_member(root, "formatted_body", priv->_formatted_body);

    MATRIX_MESSAGE_BASE_CLASS(matrix_message_text_parent_class)->to_json(matrix_message_base, json_data, &inner_error);

    if (inner_error != NULL) {
        g_propagate_error(error, inner_error);
    }
}

MatrixMessageText *
matrix_message_text_new(void) {
    MatrixMessageBase *message = matrix_message_base_construct(MATRIX_MESSAGE_TYPE_TEXT);

    matrix_message_base_set_message_type(message, "m.text");

    return MATRIX_MESSAGE_TEXT(message);
}

const gchar *
matrix_message_text_get_format(MatrixMessageText *matrix_message_text)
{
    MatrixMessageTextPrivate *priv;

    g_return_val_if_fail(matrix_message_text != NULL, NULL);

    priv = matrix_message_text_get_instance_private(matrix_message_text);

    return priv->_format;
}

void
matrix_message_text_set_format(MatrixMessageText *matrix_message_text, const gchar *format)
{
    MatrixMessageTextPrivate *priv;

    g_return_if_fail(matrix_message_text != NULL);

    priv = matrix_message_text_get_instance_private(matrix_message_text);

    if (g_strcmp0(format, priv->_format) != 0) {
        g_free(priv->_format);
        priv->_format = g_strdup(format);

        g_object_notify_by_pspec((GObject *)matrix_message_text,
                matrix_message_text_properties[PROP_FORMAT]);
    }
}

const gchar *
matrix_message_text_get_formatted_body(MatrixMessageText *matrix_message_text)
{
    MatrixMessageTextPrivate *priv;

    g_return_val_if_fail(matrix_message_text != NULL, NULL);

    priv = matrix_message_text_get_instance_private(matrix_message_text);

    return priv->_formatted_body;
}

void
matrix_message_text_set_formatted_body(MatrixMessageText *matrix_message_text, const gchar *formatted_body)
{
    MatrixMessageTextPrivate *priv;

    g_return_if_fail(matrix_message_text != NULL);

    priv = matrix_message_text_get_instance_private(matrix_message_text);

    if (g_strcmp0(formatted_body, priv->_formatted_body) != 0) {
        g_free(priv->_formatted_body);
        priv->_formatted_body = g_strdup(formatted_body);

        g_object_notify_by_pspec((GObject *)matrix_message_text,
                matrix_message_text_properties[PROP_FORMATTED_BODY]);
    }
}

static void
matrix_message_text_get_property(GObject *gobject, guint property_id, GValue *value, GParamSpec *pspec)
{
    MatrixMessageText *matrix_message_text = MATRIX_MESSAGE_TEXT(gobject);

    switch (property_id) {
        case PROP_FORMAT:
            g_value_set_string(value, matrix_message_text_get_format(matrix_message_text));
            break;
        case PROP_FORMATTED_BODY:
            g_value_set_string(value, matrix_message_text_get_formatted_body(matrix_message_text));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(gobject, property_id, pspec);
            break;
    }
}

static void matrix_message_text_set_property(GObject *gobject, guint property_id, const GValue *value, GParamSpec *pspec)
{
    MatrixMessageText *matrix_message_text = MATRIX_MESSAGE_TEXT(gobject);

    switch (property_id) {
        case PROP_FORMAT:
            matrix_message_text_set_format(matrix_message_text, g_value_get_string(value));
            break;
        case PROP_FORMATTED_BODY:
            matrix_message_text_set_formatted_body(matrix_message_text, g_value_get_string(value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(gobject, property_id, pspec);
            break;
    }
}

static void
matrix_message_text_finalize(GObject *gobject)
{
    MatrixMessageTextPrivate *priv = matrix_message_text_get_instance_private(MATRIX_MESSAGE_TEXT(gobject));

    g_free(priv->_format);
    g_free(priv->_formatted_body);

    G_OBJECT_CLASS(matrix_message_text_parent_class)->finalize(gobject);
}

static void
matrix_message_text_class_init(MatrixMessageTextClass *klass)
{
    ((MatrixMessageBaseClass *)klass)->from_json = matrix_message_text_real_from_json;
    ((MatrixMessageBaseClass *)klass)->to_json = matrix_message_text_real_to_json;
    G_OBJECT_CLASS(klass)->get_property = matrix_message_text_get_property;
    G_OBJECT_CLASS(klass)->set_property = matrix_message_text_set_property;
    G_OBJECT_CLASS(klass)->finalize = matrix_message_text_finalize;

    /**
     * MatrixMessageText:format:
     *
     * The format of the text message.
     */
    matrix_message_text_properties[PROP_FORMAT] = g_param_spec_string(
            "format", "format", "format",
            NULL,
            G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE);
    g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_FORMAT,
            matrix_message_text_properties[PROP_FORMAT]);

    /**
     * MatrixMessageText:formatted_body:
     *
     * The formatted body of the text message.
     */
    matrix_message_text_properties[PROP_FORMATTED_BODY] = g_param_spec_string(
            "formatted-body", "formatted-body", "formatted-body",
            NULL,
            G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE);
    g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_FORMATTED_BODY,
            matrix_message_text_properties[PROP_FORMATTED_BODY]);
}

static void matrix_message_text_init(MatrixMessageText *matrix_message_text)
{
    MatrixMessageTextPrivate *priv = matrix_message_text_get_instance_private(MATRIX_MESSAGE_TEXT(matrix_message_text));

    priv->_format = NULL;
    priv->_formatted_body = NULL;
}
