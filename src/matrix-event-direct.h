/*
 * This file is part of matrix-glib-sdk
 *
 * matrix-glib-sdk is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * matrix-glib-sdk is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with matrix-glib-sdk. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __MATRIX_GLIB_SDK_EVENT_DIRECT_H__
# define __MATRIX_GLIB_SDK_EVENT_DIRECT_H__

# include <glib-object.h>
# include "matrix-event-base.h"
# include "matrix-types.h"

G_BEGIN_DECLS

# define MATRIX_EVENT_TYPE_DIRECT matrix_event_direct_get_type()
G_DECLARE_DERIVABLE_TYPE(MatrixEventDirect, matrix_event_direct, MATRIX_EVENT, DIRECT, MatrixEventBase)

struct _MatrixEventDirectClass {
    MatrixEventBaseClass parent_class;
};

MatrixEventDirect* matrix_event_direct_new (void);

GSList *matrix_event_direct_get_rooms_for_userid (MatrixEventDirect *event, const gchar *user_id);
const gchar *matrix_event_direct_get_userid_for_room (MatrixEventDirect *event, const gchar *room_id);

G_END_DECLS

#endif  /* __MATRIX_GLIB_SDK_EVENT_DIRECT_H__ */
