/*
 * This file is part of matrix-glib-sdk
 *
 * matrix-glib-sdk is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * matrix-glib-sdk is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with matrix-glib-sdk. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __MATRIX_GLIB_SDK_CLIENT_SERVICE_DISCOVERY_H__
# define __MATRIX_GLIB_SDK_CLIENT_SERVICE_DISCOVERY_H__

#include <json-glib/json-glib.h>

#include "matrix-types.h"

G_BEGIN_DECLS

typedef struct {
    gboolean ready;
    MatrixServiceDiscoveryResult result;
    JsonObject *json_data;
} MatrixServiceDiscoveryData;

gboolean matrix_client_service_discovery_by_user_id(
        const gchar *user_id,
        gpointer user_data);
gboolean matrix_client_service_discovery_by_domain(
        const gchar *domain,
        gpointer user_data);

G_END_DECLS

#endif  /* __MATRIX_CLIENT_SERVICE_DISCOVERY_H__ */
