/*
 * This file is part of matrix-glib-sdk
 *
 * matrix-glib-sdk is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * matrix-glib-sdk is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with matrix-glib-sdk. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <libsoup/soup.h>

#include "matrix-types.h"
#include "matrix-client-service-discovery.h"

static void
_matrix_lookup_homeserver_callback(SoupSession *session, SoupMessage *msg, gpointer user_data)
{
    guint status_code;
    MatrixServiceDiscoveryResult res = MATRIX_SERVICE_DISCOVERY_RESULT_ERROR;
    JsonObject *root = NULL;

    g_debug("lookup_callback begin");

    g_object_get(G_OBJECT(msg),
            "status-code", &status_code,
            NULL);

    g_debug("lookup_callback code: %u", status_code);

    if (status_code == 404) {
        res = MATRIX_SERVICE_DISCOVERY_RESULT_IGNORE;
        goto cleanup;
    }

    if (status_code != 200) {
        res = MATRIX_SERVICE_DISCOVERY_RESULT_FAIL_PROMPT;
        goto cleanup;
    }

    /* Grab the response body as GBytes* */
    GBytes *msgbytes;
    gsize datasize;
    g_object_get(G_OBJECT(msg),
            "response-body-data", &msgbytes,
            NULL);
    gconstpointer data = g_bytes_get_data(msgbytes, &datasize);

    /* Try to parse the body as JSON */
    JsonParser *parser = json_parser_new();
    gboolean is_json = json_parser_load_from_data(
            parser, data, datasize, NULL);

    g_bytes_unref(msgbytes);

    if (!is_json) {
        res = MATRIX_SERVICE_DISCOVERY_RESULT_FAIL_PROMPT;
        g_object_unref(parser);
        goto cleanup;
    }

    JsonNode *content = json_parser_steal_root(parser);

    root = json_node_get_object(content);
    res = MATRIX_SERVICE_DISCOVERY_RESULT_OK;

    g_debug("got the json");

cleanup:
    /* Return the results to the caller via "user_data". */
    if (user_data != NULL) {
        MatrixServiceDiscoveryData *data =
            (MatrixServiceDiscoveryData *)user_data;

        data->result = res;
        data->json_data = root;
        data->ready = TRUE;
    }

    g_debug("lookup_callback end");
}

/**
 * matrix_client_service_discovery_by_user_id:
 * @user_id: (nullable): a valid Matrix user ID
 * @user_data: Pointer to store the .well-known/matrix/client JSON structure
 *
 * Look up "well-known" client-server information of homeserver for
 * a given Matrix user ID.
 */
gboolean
matrix_client_service_discovery_by_user_id(const gchar *user_id,
        gpointer user_data)
{
    g_return_val_if_fail(user_id != NULL, MATRIX_SERVICE_DISCOVERY_RESULT_ERROR);

    g_debug("server lookup for %s", user_id);

    /* TODO: Following user id validation tests should probably
     * eventually be put in a separate validation function. */
    if (*user_id != '@') {
        g_debug("user_id does not start with @");
        return FALSE;
    }

    if (g_strrstr(user_id, "@") != user_id) {
        g_debug("user_id has more than one @");
        return FALSE;
    }

    const gchar *colon = g_strstr_len(user_id, -1, ":");
    if (colon == NULL) {
        g_debug("user_id does not contain :");
        return FALSE;
    }

    if (g_strrstr(user_id, ":") != colon) {
        g_debug("user_id has more than one :");
        return FALSE;
    }

    if (colon - user_id <= 1) {
        g_debug("no localpart in user_id");
        return FALSE;
    }

    if (strlen(colon+1) == 0) {
        g_debug("no server name in user_id");
        return FALSE;
    }

    g_debug("validation ok: %ld", colon - user_id);

    const gchar *domain = colon + 1;

    return matrix_client_service_discovery_by_domain(domain, user_data);
}

gboolean
matrix_client_service_discovery_by_domain(const gchar *domain,
        gpointer user_data)
{
    g_return_val_if_fail(domain != NULL, FALSE);

    SoupURI *well_known_uri = soup_uri_new(NULL);
    soup_uri_set_scheme(well_known_uri, "https");
    soup_uri_set_host(well_known_uri, domain);
    soup_uri_set_path(well_known_uri, "/.well-known/matrix/client");

    SoupMessage *req = soup_message_new_from_uri("GET", well_known_uri);

    soup_uri_free(well_known_uri);

    SoupSession *soup_session = soup_session_new_with_options("ssl-strict", TRUE, NULL);

    soup_session_queue_message(soup_session, req,
            _matrix_lookup_homeserver_callback, user_data);

    g_object_unref(soup_session);

    /* Return the result code. */
    return TRUE;
}
