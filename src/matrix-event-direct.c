/*
 * This file is part of matrix-glib-sdk
 *
 * matrix-glib-sdk is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * matrix-glib-sdk is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with matrix-glib-sdk. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "matrix-event-direct.h"
#include "matrix-enumtypes.h"
#include "config.h"
#include "utils.h"

/**
 * SECTION:matrix-event-direct
 * @short_description: event to inform the client of their direct chat rooms
 *
 * Informs the client which of their joined rooms are direct chat rooms.
 */

/*
enum  {
    PROP_0,
    NUM_PROPS
};

static GParamSpec *matrix_event_direct_properties[NUM_PROPS];
*/

typedef struct {
    GHashTable *_direct_rooms;
} MatrixEventDirectPrivate;

/**
 * MatrixEventDirect:
 */
G_DEFINE_TYPE_WITH_PRIVATE(MatrixEventDirect, matrix_event_direct, MATRIX_EVENT_TYPE_BASE);

static void
matrix_event_direct_real_from_json(MatrixEventBase *matrix_event_base, JsonNode *json_data, GError **error)
{
    MatrixEventDirectPrivate *priv;
    JsonNode *content_node;
    JsonObject *root;
    JsonObject *content_root;
    GError *inner_error = NULL;

    g_return_if_fail(json_data != NULL);

    priv = matrix_event_direct_get_instance_private(MATRIX_EVENT_DIRECT(matrix_event_base));

    root = json_node_get_object(json_data);
    content_node = json_object_get_member(root, "content");
    content_root = json_node_get_object(content_node);

    // Here we convert the JSON content to a GHashTable with room IDs as keys
    // and user IDs as values.
    //
    // For reference, JSON has following format:
    // "@user1:id": [
    //     "roomid1",
    //     "roomid2"
    // ],
    // "@user2:id": [
    //     "roomid3",
    //     "roomid4"
    // ] ...

    // Get list of user IDs ...
    GList *uids = json_object_get_members(content_root);
    GList *uid;

    // ...and go through it.
    for (uid = uids; uid != NULL; uid = uid->next) {
        JsonNode *uid_node = json_object_get_member(content_root, uid->data);
        if (!JSON_NODE_HOLDS_ARRAY(uid_node)) {
            g_warning("m.direct JSON object '%s' does not hold a JSON array",
                    (gchar *)uid->data);
            continue;
        }

        // Get list of room IDs...
        JsonArray *roomids_array = json_node_get_array(uid_node);
        GList *roomid_nodes = json_array_get_elements(roomids_array);
        GList *roomid_node;

        // ...and go through it, ...
        for (roomid_node = roomid_nodes;
                roomid_node != NULL;
                roomid_node = roomid_node->next) {
            if (!JSON_NODE_HOLDS_VALUE(roomid_node->data)) {
                g_warning("Malformed array in m.direct JSON under object '%s'",
                        (gchar *)uid->data);
                continue;
            }

            // ..adding each roomid->userid pair into the hash table.
            const gchar *roomid = json_node_get_string(roomid_node->data);
            g_hash_table_insert(priv->_direct_rooms,
                    g_strdup(roomid), g_strdup(uid->data));
        }
    }

    MATRIX_EVENT_BASE_CLASS(matrix_event_direct_parent_class)->from_json(matrix_event_base, json_data, &inner_error);

    if ((inner_error != NULL)) {
        g_propagate_error(error, inner_error);
    }
}

static gint
_find_json_string(gconstpointer a, gconstpointer b)
{
    JsonNode *node = (JsonNode *)a;

    return g_strcmp0(json_node_get_string(node), (const gchar *)b);
}

static void
matrix_event_direct_real_to_json(MatrixEventBase *matrix_event_base, JsonNode *json_data, GError **error)
{
    MatrixEventDirectPrivate *priv;
    JsonNode *content_node;
    JsonObject *root;
    JsonObject *content_root;
    GError *inner_error = NULL;

    g_return_if_fail(json_data != NULL);

    priv = matrix_event_direct_get_instance_private(MATRIX_EVENT_DIRECT(matrix_event_base));

    root = json_node_get_object(json_data);
    content_node = json_object_get_member(root, "content");
    content_root = json_node_get_object(content_node);

    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, priv->_direct_rooms);

    while (g_hash_table_iter_next(&iter, &key, &value)) {
        const gchar *room_id = key;
        const gchar *user_id = value;

        JsonArray *users;
        if (json_object_has_member(content_root, user_id)) {
            users = json_object_get_array_member(content_root, user_id);
        } else {
            JsonNode *user_node = json_node_new(JSON_TYPE_ARRAY);
            users = json_node_get_array(user_node);
            json_object_set_array_member(content_root, user_id, users);
        }

        if (!g_list_find_custom(json_array_get_elements(users),
                    room_id,
                    _find_json_string)) {
            json_array_add_string_element(users, room_id);
        }
    }

    MATRIX_EVENT_BASE_CLASS(matrix_event_direct_parent_class)->to_json(matrix_event_base, json_data, &inner_error);

    if (inner_error != NULL) {
        g_propagate_error(error, inner_error);
    }
}

/**
 * matrix_event_direct_new:
 *
 * Create a new #MatrixEventDirect object.
 *
 * Returns: (transfer full): a new #MatrixEventDirect object
 */
MatrixEventDirect *
matrix_event_direct_new(void)
{
    return (MatrixEventDirect *)matrix_event_base_construct(MATRIX_EVENT_TYPE_DIRECT);
}


/**
 * matrix_event_direct_get_rooms_for_userid:
 * @event: a #MatrixEventDirect
 * @user_id: Matrix user ID string
 *
 * Get the list of IDs of direct chat room we have with the user_id
 *
 * The returned value is owned by @event and should not be freed.
 *
 * Returns: (transfer none) (element-type GSList): a #GSList containing room IDs
 */
GSList *
matrix_event_direct_get_rooms_for_userid(MatrixEventDirect *matrix_event_direct, const gchar *user_id)
{
    MatrixEventDirectPrivate *priv;

    g_return_val_if_fail(matrix_event_direct != NULL, NULL);

    priv = matrix_event_direct_get_instance_private(matrix_event_direct);

    GSList *roomlist = NULL;
    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, priv->_direct_rooms);

    while (g_hash_table_iter_next(&iter, &key, &value)) {
        if (!g_strcmp0((const gchar *)value, user_id)) {
            roomlist = g_slist_prepend(roomlist, key);
        }
    }

    return roomlist;
}


const gchar *
matrix_event_direct_get_userid_for_room(MatrixEventDirect *matrix_event_direct, const gchar *room_id)
{
    MatrixEventDirectPrivate *priv;

    g_return_val_if_fail(matrix_event_direct != NULL, NULL);

    priv = matrix_event_direct_get_instance_private(matrix_event_direct);

    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, priv->_direct_rooms);

    while (g_hash_table_iter_next(&iter, &key, &value)) {
        if (!g_strcmp0(key, room_id)) {
            return ((const gchar *)value);
        }
    }

    return NULL;
}


static void
matrix_event_direct_get_property(GObject *gobject, guint property_id, GValue* value, GParamSpec* pspec)
{
    // No properties
}

static void
matrix_event_direct_set_property(GObject *gobject, guint property_id, const GValue* value, GParamSpec* pspec)
{
    // No properties
}

static void
matrix_event_direct_finalize(GObject *gobject)
{
    MatrixEventDirectPrivate *priv = matrix_event_direct_get_instance_private(MATRIX_EVENT_DIRECT(gobject));

    g_hash_table_unref(priv->_direct_rooms);

    G_OBJECT_CLASS(matrix_event_direct_parent_class)->finalize(gobject);
}

static void
matrix_event_direct_class_init(MatrixEventDirectClass *klass)
{
    ((MatrixEventBaseClass *)klass)->from_json = matrix_event_direct_real_from_json;
    ((MatrixEventBaseClass *)klass)->to_json = matrix_event_direct_real_to_json;
    G_OBJECT_CLASS (klass)->get_property = matrix_event_direct_get_property;
    G_OBJECT_CLASS (klass)->set_property = matrix_event_direct_set_property;
    G_OBJECT_CLASS (klass)->finalize = matrix_event_direct_finalize;
}

static void
matrix_event_direct_init(MatrixEventDirect *matrix_event_direct) {
    MatrixEventDirectPrivate *priv = matrix_event_direct_get_instance_private(matrix_event_direct);

    priv->_direct_rooms = g_hash_table_new_full(
            g_str_hash,
            g_str_equal,
            g_free,
            g_free);
}
