#include "matrix-event-direct.h"

const gchar *raw_json = "\
            {\
              \"type\": \"m.direct\",\
              \"content\": {\
                \"@user1:foo\": [\
                  \"!room1:foo\",\
                  \"!room2:foo\",\
                  \"!room3:foo\"\
                ],\
                \"@user2:bar\": [\
                  \"!room3:bar\",\
                  \"!room4:bar\"\
                ]\
              }\
            }\
";

gint _findstr(gconstpointer a, gconstpointer b)
{
    return 0;
}

int main()
{
    GError *err = NULL;
    JsonParser *parser = json_parser_new();

    if (!json_parser_load_from_data(parser, raw_json, -1, &err)) {
        g_object_unref(parser);
        g_warning("could not parse json: %s", err->message);
        return 1;
    }
    JsonNode *root_node = json_parser_steal_root(parser);
    g_object_unref(parser);

    // matrix_event_base_new_from_json
    MatrixEventBase *event = matrix_event_base_new_from_json(NULL, root_node, &err);
    json_node_unref(root_node);

    if (err != NULL) {
        g_warning("could not  %s", err->message);
        return 1;
    }

    g_return_val_if_fail(event != NULL, 1);

    const gchar *type = matrix_event_base_get_event_type(event);
    g_return_val_if_fail(type != NULL, 1);

    if (strcmp(type, "m.direct") != 0) {
        g_warning("Expected type '%s', got '%s'\n", "m.direct", type);
        return 1;
    }

    // matrix_event_direct_get_rooms_for_userid
    GSList *rooms;

    rooms = matrix_event_direct_get_rooms_for_userid(MATRIX_EVENT_DIRECT(event), "@user1:foo");
    g_return_val_if_fail(g_slist_length(rooms) == 3, 1);

    g_return_val_if_fail(
            g_slist_find_custom(rooms, "!room1:foo", _findstr) != NULL, 1);
    g_return_val_if_fail(
            g_slist_find_custom(rooms, "!room2:foo", _findstr) != NULL, 1);
    g_return_val_if_fail(
            g_slist_find_custom(rooms, "!room3:foo", _findstr) != NULL, 1);
    g_slist_free(rooms);

    rooms = matrix_event_direct_get_rooms_for_userid(MATRIX_EVENT_DIRECT(event), "@user2:bar");
    g_return_val_if_fail(g_slist_length(rooms) == 2, 1);

    g_return_val_if_fail(
            g_slist_find_custom(rooms, "!room3:bar", _findstr) != NULL, 1);
    g_return_val_if_fail(
            g_slist_find_custom(rooms, "!room4:bar", _findstr) != NULL, 1);
    g_slist_free(rooms);

    rooms = matrix_event_direct_get_rooms_for_userid(MATRIX_EVENT_DIRECT(event), "@xuser:bar");
    g_return_val_if_fail(g_slist_length(rooms) == 0, 1);
    g_slist_free(rooms);


    // matrix_event_direct_get_userid_for_room
    const gchar *user_id;

    user_id = matrix_event_direct_get_userid_for_room(MATRIX_EVENT_DIRECT(event), "!room1:foo");
    g_return_val_if_fail(user_id != NULL, 1);
    g_return_val_if_fail(!g_strcmp0(user_id, "@user1:foo"), 1);

    return 0;
}

