#include "matrix-message-base.h"

#define MSGTYPE "m.text"
#define BODY "hello world"

const gchar *raw_json = "\
            {\
              \"msgtype\": \""MSGTYPE"\",\
              \"body\": \""BODY"\"\
            }\
";

MatrixMessageBase *message = NULL;

void
test_from_json()
{
    GError *err = NULL;
    JsonParser *parser = json_parser_new();

    if (!json_parser_load_from_data(parser, raw_json, -1, &err)) {
        g_object_unref(parser);
        g_error("could not parse json: %s", err->message);
        g_error_free(err);
        return;
    }
    JsonNode *root_node = json_parser_steal_root(parser);
    g_object_unref(parser);

    message = matrix_message_base_new_from_json(root_node, &err);
    json_node_unref(root_node);

    if (err != NULL) {
        g_error("could not create message: %s", err->message);
        g_error_free(err);
        return;
    }

    g_assert_nonnull(message);
    g_assert_true(MATRIX_MESSAGE_IS_BASE(message));
}

#define MSGTYPE2 "m.emote"
void
test_message_type()
{
    g_assert_nonnull(message);
    g_assert_cmpstr(matrix_message_base_get_message_type(message), ==, MSGTYPE);

    matrix_message_base_set_message_type(message, MSGTYPE2);

    g_assert_cmpstr(matrix_message_base_get_message_type(message), ==, MSGTYPE2);
}


#define BODY2 "says hello"
void
test_body()
{
    g_assert_nonnull(message);
    g_assert_cmpstr(matrix_message_base_get_body(message), ==, BODY);

    matrix_message_base_set_body(message, BODY2);

    g_assert_cmpstr(matrix_message_base_get_body(message), ==, BODY2);
}


void
test_to_json()
{
    GError *err = NULL;
    JsonObject *obj = json_object_new();
    JsonNode *node = json_node_new(JSON_NODE_OBJECT);
    JsonNode *n;

    node = json_node_init_object(node, obj);
    json_object_unref(obj);

    matrix_message_base_to_json(message, node, &err);
    if (err != NULL) {
        g_error(err->message);
        g_error_free(err);
        return;
    }

    g_assert_true(json_object_has_member(obj, "msgtype"));
    n = json_object_get_member(obj, "msgtype");
    g_assert_true(JSON_NODE_HOLDS_VALUE(n));
    g_assert_cmpstr(json_node_get_string(n), ==, MSGTYPE2);

    g_assert_true(json_object_has_member(obj, "body"));
    n = json_object_get_member(obj, "body");
    g_assert_true(JSON_NODE_HOLDS_VALUE(n));
    g_assert_cmpstr(json_node_get_string(n), ==, BODY2);
}


int
main (int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/Message/base/from_json", test_from_json);
    g_test_add_func("/Message/base/message_type", test_message_type);
    g_test_add_func("/Message/base/body", test_body);
    g_test_add_func("/Message/base/to_json", test_to_json);

    return g_test_run();
}
