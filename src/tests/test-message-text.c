#include "matrix-message-text.h"

#define MSGTYPE "m.text"
#define BODY "hello world"
#define FORMAT "format"
#define FORMATTED_BODY "formatted body"

const gchar *raw_json = "\
            {\
              \"msgtype\": \""MSGTYPE"\",\
              \"body\": \""BODY"\",\
              \"format\": \""FORMAT"\",\
              \"formatted_body\": \""FORMATTED_BODY"\"\
            }\
";

MatrixMessageBase *message = NULL;

void
test_from_json()
{
    GError *err = NULL;
    JsonParser *parser = json_parser_new();

    if (!json_parser_load_from_data(parser, raw_json, -1, &err)) {
        g_object_unref(parser);
        g_error("could not parse json: %s", err->message);
        g_error_free(err);
        return;
    }
    JsonNode *root_node = json_parser_steal_root(parser);
    g_object_unref(parser);

    message = matrix_message_base_new_from_json(root_node, &err);
    json_node_unref(root_node);

    if (err != NULL) {
        g_error("could not create message: %s", err->message);
        g_error_free(err);
        return;
    }

    g_assert_nonnull(message);
    g_assert_true(MATRIX_MESSAGE_IS_TEXT(message));
}

#define FORMAT2 "otherformat"
void
test_format()
{
    MatrixMessageText *tmessage = MATRIX_MESSAGE_TEXT(message);

    g_assert_nonnull(tmessage);

    g_assert_cmpstr(matrix_message_text_get_format(tmessage), ==, FORMAT);
    matrix_message_text_set_format(tmessage, FORMAT2);
    g_assert_cmpstr(matrix_message_text_get_format(tmessage), ==, FORMAT2);
}


#define FORMATTED_BODY2 "hi world"
void
test_formatted_body()
{
    MatrixMessageText *tmessage = MATRIX_MESSAGE_TEXT(message);

    g_assert_nonnull(tmessage);

    g_assert_cmpstr(matrix_message_text_get_formatted_body(tmessage), ==, FORMATTED_BODY);
    matrix_message_text_set_formatted_body(tmessage, FORMATTED_BODY2);
    g_assert_cmpstr(matrix_message_text_get_formatted_body(tmessage), ==, FORMATTED_BODY2);
}


void
test_to_json()
{
    GError *err = NULL;
    JsonObject *obj = json_object_new();
    JsonNode *node = json_node_new(JSON_NODE_OBJECT);
    JsonNode *n;

    node = json_node_init_object(node, obj);
    json_object_unref(obj);

    matrix_message_base_to_json(message, node, &err);
    if (err != NULL) {
        g_error(err->message);
        g_error_free(err);
        return;
    }

    g_assert_true(json_object_has_member(obj, "format"));
    n = json_object_get_member(obj, "format");
    g_assert_true(JSON_NODE_HOLDS_VALUE(n));
    g_assert_cmpstr(json_node_get_string(n), ==, FORMAT2);

    g_assert_true(json_object_has_member(obj, "formatted_body"));
    n = json_object_get_member(obj, "formatted_body");
    g_assert_true(JSON_NODE_HOLDS_VALUE(n));
    g_assert_cmpstr(json_node_get_string(n), ==, FORMATTED_BODY2);
}


int
main (int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/Message/text/from_json", test_from_json);
    g_test_add_func("/Message/text/format", test_format);
    g_test_add_func("/Message/text/formatted_body", test_formatted_body);
    g_test_add_func("/Message/text/to_json", test_to_json);

    return g_test_run();
}
