#include "matrix-event-room-message.h"

#define SENDER "@tester:example.com"
#define MSGTYPE "m.text"
#define BODY "hello world"
#define EVENTID "$12345:example.com"
#define ROOMID "!fakeroom:example.com"
#define TS "1624316921011"

const gchar *raw_json = "\
            {\
              \"type\": \"m.room.message\",\
              \"sender\": \""SENDER"\",\
              \"content\": {\
                \"msgtype\": \""MSGTYPE"\",\
                \"body\": \""BODY"\"\
              },\
              \"event_id\": \""EVENTID"\",\
              \"room_id\": \""ROOMID"\",\
              \"origin_server_ts\": "TS",\
              \"unsigned\": {\
                \"age\": 88589306,\
                \"transaction_id\": \"m1624316920780.207\"\
              }\
            }\
";

void
test_from_json()
{
    GError *err = NULL;
    JsonParser *parser = json_parser_new();
    MatrixEventBase *event = NULL;

    if (!json_parser_load_from_data(parser, raw_json, -1, &err)) {
        g_object_unref(parser);
        g_error("could not parse json: %s", err->message);
        g_error_free(err);
        return;
    }
    JsonNode *root_node = json_parser_steal_root(parser);
    g_object_unref(parser);

    event = matrix_event_base_new_from_json(NULL, root_node, &err);
    json_node_unref(root_node);

    if (err != NULL) {
        g_error("could not create event: %s", err->message);
        g_error_free(err);
        return;
    }

    g_assert_nonnull(event);
    g_assert_cmpstr(matrix_event_base_get_event_type(event), ==, "m.room.message");
    g_assert_true(MATRIX_EVENT_IS_BASE(event));
    g_assert_true(MATRIX_EVENT_IS_ROOM(event));
    g_assert_true(MATRIX_EVENT_IS_ROOM_MESSAGE(event));

    g_assert_cmpstr(matrix_event_room_get_event_id(MATRIX_EVENT_ROOM(event)), ==, EVENTID);
    g_assert_cmpstr(matrix_event_room_get_room_id(MATRIX_EVENT_ROOM(event)), ==, ROOMID);
    g_assert_cmpstr(matrix_event_room_get_sender(MATRIX_EVENT_ROOM(event)), ==, SENDER);
    g_assert_cmpint(matrix_event_room_get_age(MATRIX_EVENT_ROOM(event)), ==, 88589306);
    g_assert_null(matrix_event_room_get_redacted_because(MATRIX_EVENT_ROOM(event)));
    g_assert_cmpstr(matrix_event_room_get_transaction_id(MATRIX_EVENT_ROOM(event)), ==, "m1624316920780.207");

    g_assert_cmpstr(matrix_event_room_message_get_body(MATRIX_EVENT_ROOM_MESSAGE(event)), ==, BODY);
}

void
test_from_scratch()
{
    // TODO
}

int
main (int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/EventRoomMessage/m.text/from_json", test_from_json);
    g_test_add_func("/EventRoomMessage/m.text/from_scratch", test_from_scratch);

    return g_test_run();
}
